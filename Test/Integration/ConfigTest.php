<?php
/**
 * @package  Digibart\CheckoutOptionalAgreements
 * @author Bartosz Herba <bartoszherba@gmail.pl>
 */

namespace Digibart\CheckoutOptionalAgreements\Test\Integration;

use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\Component\ComponentRegistrarInterface;
use Magento\Framework\Module\ModuleList;
use Magento\TestFramework\Helper\Bootstrap;

/**
 * Class ConfigTest
 */
class ConfigTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var string
     */
    protected $moduleName = 'Digibart_CheckoutOptionalAgreements';

    public function testModuleIsConfigured()
    {
        /** @var ModuleList $moduleList */
        $moduleList = Bootstrap::getObjectManager()->create(ModuleList::class);
        $this->assertTrue($moduleList->has($this->moduleName));
    }

    public function testModuleIsRegistered()
    {
        /** @var ComponentRegistrarInterface $componentRegistrar */
        $componentRegistrar = Bootstrap::getObjectManager()->create(ComponentRegistrarInterface::class);
        $this->assertArrayHasKey($this->moduleName, $componentRegistrar->getPaths(ComponentRegistrar::MODULE));
    }
}
